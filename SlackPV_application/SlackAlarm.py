__author__ = "Miklos Boros"
__credits__ = ["Miklos Boros", "Benjamin Bertrand", "Anders Harrisson"]
__license__ = "GPL"
__version__ = "1.0.1"
__release__ = "23.09.2020"
__maintainer__ = "Miklos Boros"
__email__ = ["miklos.boros@ess.eu", "borosmiklos@gmail"]
__status__ = "Production"

import requests
import argparse
import sys
import configparser
from datetime import datetime
import os


def post_message_to_slack(slack_config, channelname, text, blocks=None):
    return requests.post(
        "https://ess-eric.slack.com/api/chat.postMessage",
        {
            "token": slack_config["slack_token"],
            "channel": channelname,
            "text": text,
            "icon_url": slack_config["slack_icon_url"],
            "username": slack_config["slack_user_name"],
        },
    )


def report_error_on_slack(slack_config, channelname, slack_error_message):
    try:
        dateTimeObj = datetime.now()
        timestampStr = dateTimeObj.strftime("%d-%b-%Y (%H:%M:%S)")
        print(
            post_message_to_slack(
                slack_config,
                "#﻿alarmservice_notification_debugging",
                timestampStr + "  " + slack_error_message,
            )
        )
    except Exception as e:
        print("   SlackAlarm Error!: Slack API error: ", e)


def main(argv):

    slack_config = {
        "slack_token": "",
        "slack_icon_url": "",
        "slack_user_name": "",
        "configurationRepoUrl": "",
    }
    print("**********SlackAlarm Entry**************")

    slack_channel = "#cryo_alarmservice_notification"

    parser = argparse.ArgumentParser(
        description="Sending a Slack message from the Alarm Service. This python3 script meant to be called by the Alarm Service, when the selected PV is in alarm state."
    )
    parser.add_argument(
        "LocalConfigFile",
        metavar="LocalConfigurationFile",
        type=str,
        help="The configuration file in ini format that contains the Slack token, Slack APP icon, Slack App user name and the GitLab repository url",
    )
    parser.add_argument(
        "AlarmConfigFile",
        metavar="AlarmConfigurationFile",
        type=str,
        help="The configuration file in the SlakAlarm GitLab repository with extension.",
    )
    parser.add_argument(
        "TriggerPV",
        metavar="TriggerPV",
        type=str,
        help="PV name that was triggered by the AlarmService",
    )
    parser.add_argument(
        "PVSeverity",
        metavar="PVSeverity",
        type=str,
        help="Severity of the TriggerPV (MINOR/MAJOR)",
    )

    args = parser.parse_args()

    if os.path.isfile(args.LocalConfigFile):

        config = configparser.ConfigParser()
        config.read(args.LocalConfigFile)
        if (
            config.has_section("SlackAlarmConfiguration")
            and config.has_option("SlackAlarmConfiguration", "SLACK_TOKEN")
            and config.has_option("SlackAlarmConfiguration", "SLACK_APP_ICON")
            and config.has_option("SlackAlarmConfiguration", "SLACK_APP_USER")
            and config.has_option("SlackAlarmConfiguration", "CONFIG_REPO")
        ):
            slack_config["slack_token"] = config["SlackAlarmConfiguration"][
                "SLACK_TOKEN"
            ]
            slack_config["slack_icon_url"] = config["SlackAlarmConfiguration"][
                "SLACK_APP_ICON"
            ]
            slack_config["slack_user_name"] = config["SlackAlarmConfiguration"][
                "SLACK_APP_USER"
            ]
            slack_config["configurationRepoUrl"] = config["SlackAlarmConfiguration"][
                "CONFIG_REPO"
            ]
            parseOK = True
            try:
                response = requests.get(
                    slack_config["configurationRepoUrl"] + args.AlarmConfigFile,
                    stream=True,
                    timeout=5,
                )
            except Exception as e:
                print(
                    "   SlackAlarm Error!: We failed to reach the configuration repository. "
                    + slack_config["configurationRepoUrl"]
                )
                print("   SlackAlarm Error!: Reason: ", e)
            else:
                try:
                    config.read_string(response.text)
                except Exception as e:
                    print(
                        "   SlackAlarm Error!: in ("
                        + args.AlarmConfigFile
                        + ") Reason: ",
                        e,
                    )
                    parseOK = False
                    report_error_on_slack(
                        slack_config,
                        slack_channel,
                        "   SlackAlarm Error!: in ("
                        + args.AlarmConfigFile
                        + ") Reason: "
                        + str(e),
                    )
                if parseOK:
                    if config.has_section(args.TriggerPV):
                        print("   SlackAlarm: Alarm PV found:" + args.TriggerPV)
                        if config.has_option(args.TriggerPV, "SLACK_CHANNEL"):
                            slack_channel = config[args.TriggerPV]["SLACK_CHANNEL"]
                            print("   SlackAlarm: Channel selected:" + slack_channel)
                            messageOK = False
                            if config.has_option(args.TriggerPV, "MAJOR_MESSAGE"):
                                messageOK = True
                                if args.PVSeverity == "MAJOR":
                                    message = config[args.TriggerPV]["MAJOR_MESSAGE"]
                                    print(
                                        "   SlackAlarm: Sending message for MAJOR Severity:"
                                    )
                                    dateTimeObj = datetime.now()
                                    timestampStr = dateTimeObj.strftime(
                                        "%d-%b-%Y (%H:%M:%S)"
                                    )
                                    try:
                                        print(
                                            post_message_to_slack(
                                                slack_config,
                                                slack_channel,
                                                timestampStr + "  " + message,
                                            )
                                        )
                                    except Exception as e:
                                        print(
                                            "   SlackAlarm Error!: Slack API error: ", e
                                        )
                            if config.has_option(args.TriggerPV, "MINOR_MESSAGE"):
                                messageOK = True
                                if args.PVSeverity == "MINOR":
                                    message = config[args.TriggerPV]["MINOR_MESSAGE"]
                                    print(
                                        "   SlackAlarm: Sending message for MINOR Severity:"
                                    )
                                    dateTimeObj = datetime.now()
                                    timestampStr = dateTimeObj.strftime(
                                        "%d-%b-%Y (%H:%M:%S)"
                                    )
                                    try:
                                        print(
                                            post_message_to_slack(
                                                slack_config,
                                                slack_channel,
                                                timestampStr + "  " + message,
                                            )
                                        )
                                    except Exception as e:
                                        print(
                                            "   SlackAlarm Error!: Slack API error: ", e
                                        )
                            if config.has_option(args.TriggerPV, "INVALID_MESSAGE"):
                                messageOK = True
                                if args.PVSeverity == "INVALID":
                                    message = config[args.TriggerPV]["INVALID_MESSAGE"]
                                    print(
                                        "   SlackAlarm: Sending message for INVALID Severity:"
                                    )
                                    dateTimeObj = datetime.now()
                                    timestampStr = dateTimeObj.strftime(
                                        "%d-%b-%Y (%H:%M:%S)"
                                    )
                                    try:
                                        print(
                                            post_message_to_slack(
                                                slack_config,
                                                slack_channel,
                                                timestampStr + "  " + message,
                                            )
                                        )
                                    except Exception as e:
                                        print(
                                            "   SlackAlarm Error!: Slack API error: ", e
                                        )
                            if config.has_option(
                                args.TriggerPV, "DISCONNECTED_MESSAGE"
                            ):
                                messageOK = True
                                if args.PVSeverity == "UNDEFINED":
                                    message = config[args.TriggerPV][
                                        "DISCONNECTED_MESSAGE"
                                    ]
                                    print(
                                        "   SlackAlarm: Sending message for DISCONNECTED Severity:"
                                    )
                                    dateTimeObj = datetime.now()
                                    timestampStr = dateTimeObj.strftime(
                                        "%d-%b-%Y (%H:%M:%S)"
                                    )
                                    try:
                                        print(
                                            post_message_to_slack(
                                                slack_config,
                                                slack_channel,
                                                timestampStr + "  " + message,
                                            )
                                        )
                                    except Exception as e:
                                        print(
                                            "   SlackAlarm Error!: Slack API error: ", e
                                        )
                            if not messageOK:
                                print(
                                    "   SlackAlarm Error!: MAJOR_MESSAGE/MINOR_MESSAGE/INVALID_MESSAGE/DISCONNECTED_MESSAGE is not defined"
                                )
                                report_error_on_slack(
                                    slack_config,
                                    slack_channel,
                                    "   SlackAlarm Error!: PV "
                                    + args.TriggerPV
                                    + " in ("
                                    + args.AlarmConfigFile
                                    + ") Reason: At least one of MAJOR_MESSAGE/MINOR_MESSAGE/INVALID_MESSAGE/DISCONNECTED_MESSAGE must be defined!",
                                )

                        else:
                            print("   SlackAlarm Error!: SLACK_CHANNEL not defined")
                            report_error_on_slack(
                                slack_config,
                                slack_channel,
                                "   SlackAlarm Error!: PV "
                                + args.TriggerPV
                                + " in ("
                                + args.AlarmConfigFile
                                + ") Reason: SLACK_CHANNEL not defined!",
                            )

                    else:
                        print(
                            "   SlackAlarm Error!: PV not found: "
                            + args.TriggerPV
                            + "@"
                            + args.AlarmConfigFile
                        )
                        report_error_on_slack(
                            slack_config,
                            slack_channel,
                            "   SlackAlarm Error!: PV "
                            + args.TriggerPV
                            + " in ("
                            + args.AlarmConfigFile
                            + ") is not defined!",
                        )

        else:
            print("   SlackAlarm Error!: local configuration file syntax error!")

    else:
        print("   SlackAlarm Error!: can not find local configuration file!")

    print("**********SlackAlarm Exit**************")


if __name__ == "__main__":
    main(sys.argv[1:])
